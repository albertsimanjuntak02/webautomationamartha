## Web Automation
Simple API and Web Automation using Ruby and cucumber
​
## Setup
​
### Dependencies
- RVM
- Ruby 2.5.1
- Git
- Bundler
​
### Install RVM & Ruby
1. RVM
​
  ```
  curl -L get.rvm.io | bash -s stable
  source ~/.bash_profile
  ```
​
  Then run `rvm requirements` and follow the instructions
​
2. Ruby 2.5.1
​
  ```
  rvm install 2.5.1
  ```
​
## Setup Gem
​
  ```
    rvm use 2.5.1@WebAutomationAmartha --create
    gem install bundler
    bundle install
  ```

## Run scenario
  ```
    cucumber --tag <@tag_id>
  ```

## Setup Project
  ```
    copy and paste file environment/environment.env to root of project then rename become ".env"
  ```

## Result

![Screen_Recording_2022-09-25_at_16.55.05](/uploads/d5f89105e055598a263df417e170cab7/Screen_Recording_2022-09-25_at_16.55.05.mov)

![Screen_Shot_2022-09-25_at_17.04.52](/uploads/beda344ab3e2d93d719f21cd206d12e6/Screen_Shot_2022-09-25_at_17.04.52.png)

![Screen_Shot_2022-09-25_at_17.05.03](/uploads/b53af01638caf4b5fc0ba8a6674c495d/Screen_Shot_2022-09-25_at_17.05.03.png)

![Screen_Shot_2022-09-25_at_17.05.33](/uploads/f84fb92f6fab9f9046a6b303461a3f24/Screen_Shot_2022-09-25_at_17.05.33.png)
