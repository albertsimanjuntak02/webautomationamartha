@web-automation @login
Feature: Login - Web Automation

  @login @positive @valid_credential
  Scenario: Login to Sauce Demo Website with valid credential
    Given user is on login page
    When user enter a valid credential using username "standard_user" and password "secret_sauce"
    Then website home page will have displayed

  @login @negative @invalid_credential
  Scenario Outline: Login to Sauce Demo Website with invalid credential
    Given user is on login page
    When user enter a valid credential using username "<username>" and password "<password>"
    Then user will be displayed error message for invalid credential

    Examples:
      | username      | password     |
      # Invalid username and password
      | standar       | password123  |
      # valid username and invalid password
      | standard_user | password123  |
      # Invalid username and valid password
      | standar       | secret_sauce |

  @login @negative @invalid_credential @empty_credential
  Scenario Outline: Login to Sauce Demo Website with empty credential
    Given user is on login page
    When user enter a valid credential using username "<username>" and password "<password>"
    Then user will be displayed empty credential for "<error_message>"

    Examples:
      | username      | password     | error_message       |
      # Invalid username and password
      |               | secret_sauce | error_username      |
      # valid username and invalid password
      | standard_user |              | error_password      |
      # Invalid username and valid password
      |               |              | error_empty_credential |

