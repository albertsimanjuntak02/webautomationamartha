@web-automation @checkout
Feature: Checkout - Web Automation

  Background:
    Given user is on login page
    And user enter a valid credential using username "standard_user" and password "secret_sauce"
    And website home page will have displayed
    And user choose filter using "z_to_a"
    When user add product "Sauce Labs Onesie" to cart
    Then user go to checkout page

  @positive @checkout
  Scenario: Login to Sauce Demo Website with valid credential
    And user validate product name "Sauce Labs Onesie" is in checkout page
    And user click checkout button
    When user fill field with value first name "albert", last name "last name", and postal code "123"
    And user review product "Sauce Labs Onesie" is in Overview
    And user click finish button
    Then user validate checkout complete