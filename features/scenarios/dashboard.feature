@web-automation @dashboard
Feature: Dashboard - Web Automation

  Background:
    Given user is on login page
    When user enter a valid credential using username "standard_user" and password "secret_sauce"
    Then website home page will have displayed

  @dashboard @positive @list_product
  Scenario Outline: Explore filter in dashboard page
    And user click icon filter in dashboard page
    When user choose filter using "<type_filter>"
    Then user will see list of product

    Examples:
      | type_filter |
      | a_to_z      |
      | z_to_a      |
      | low_to_high |
      | high_to_low |