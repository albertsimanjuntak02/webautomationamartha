Then(/^website home page will have displayed$/) do
  @browser = DashboardPage.new
  expect(@browser).to have_inventory_item_section
  expect(@browser).to have_cart_container
end

Then("user click icon filter in dashboard page") do
  expect(@browser).to have_icon_filter
  @browser.icon_filter.click
end

And(/^user choose filter using "([^"]*)"$/) do |type_filter|
  @browser.click_category_filter(type_filter)
end

Then("user will see list of product") do
  expect(@browser).to have_inventory_item_section
end

And(/^user add product "([^"]*)" to cart$/) do |product|
  @browser.add_product_to_cart(product)
end

Then("user go to checkout page") do
  expect(@browser).to have_icon_cart
  @browser.icon_cart.click
end