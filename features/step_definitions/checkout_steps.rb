And(/^user validate product name "([^"]*)" is in checkout page$/
) do |product|
  @browser = CheckoutPage.new
  @browser.validate_product(product)
end

And(/^user add item product to "([^"]*)"$/) do |amount|
  @browser.quantity_field.set(amount)
end

And("user click checkout button") do
  @browser.checkout_button.click
end

When(/^user fill field with value first name "([^"]*)", last name "([^"]*)", and postal code "([^"]*)"$/
) do |first_name, last_name, postal_code|
  @browser.input_first_name_field.send_keys(first_name)
  @browser.input_last_name_field.send_keys(last_name)
  @browser.input_postal_code_field.send_keys(postal_code)
  @browser.continue_button.click
end

And(/^user review product "([^"]*)" is in Overview$/) do |product|
  @browser.overview_product(product)
end

And("user click finish button") do
  @browser.finish_button.click
end

Then("user validate checkout complete") do
  expect(@browser).to have_checkout_complete_txt
  expect(@browser).to have_complete_header_txt
end