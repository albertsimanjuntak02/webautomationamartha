
Given(/^user is on login page$/) do
  @browser = LoginPage.new
  @browser.load
  @browser.wait_until_login_logo_visible(wait: 20)
  expect(@browser).to have_text('Accepted usernames are:')
  expect(@browser).to have_input_username_field
end

When(/^user enter a valid credential using username "([^"]*)" and password "([^"]*)"$/
) do |username, password|
  @browser.input_username_field.send_keys(username)
  @browser.input_password_field.send_keys(password)
  @browser.login_button.click
end

Then("user will be displayed error message for invalid credential") do
  expect(@browser).to have_login_error_invalid_credential
end

When(/^user will be displayed empty credential for "([^"]*)"$/
) do |component|
    @browser.invalid_error_message(component)
end