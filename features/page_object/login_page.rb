# frozen_string_literal: true

class LoginPage < SitePrism::Page
  path = ENV['WEB_BASE_URL'] + '/'
  set_url(path)

  element :login_logo, '.login_logo'
  element :input_username_field, '#user-name'
  element :input_password_field, '#password'
  element :login_button, '#login-button'
  element :login_error_invalid_credential, :xpath, "//*[contains(text(),'Epic sadface: Username and password do not match')]"
  element :login_error_empty_username, :xpath, "//*[contains(text(),'Epic sadface: Username is required')]"
  element :login_error_empty_password, :xpath, "//*[contains(text(),'Epic sadface: Password is required')]"


  def invalid_error_message(component)
    if component == "error_username"
      wait_until_login_error_empty_username_visible
    elsif component == "error_password"
      wait_until_login_error_empty_password_visible
    elsif component == "error_empty_credential"
      wait_until_login_error_empty_username_visible
    end
  end
end

