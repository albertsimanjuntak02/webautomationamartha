# frozen_string_literal: true

class DashboardPage < SitePrism::Page
  element :inventory_item_section, '.inventory_item'
  element :cart_container, '#shopping_cart_container'
  element :icon_filter, :xpath, "//*[@data-test='product_sort_container']"
  element :filter_A_to_Z, :xpath, "//*[contains(@value,'az')]"
  element :filter_Z_to_A, :xpath, "//*[contains(text(),'Name (Z to A)')]"
  element :filter_low_to_high, :xpath, "//*[contains(text(),'Price (low to high)')]"
  element :filter_high_to_low, :xpath, "//*[contains(text(),'Price (high to low)')]"
  element :add_to_cart_button, :xpath, "//*[contains(text(),'Add to cart')]"
  element :icon_cart, :xpath, "//*[@id='shopping_cart_container']/a"

  def click_category_filter(category)
    if category == "a_to_z"
        filter_A_to_Z.click
    elsif category == "z_to_a"
        filter_Z_to_A.click
    elsif category == "low_to_high"
        filter_low_to_high.click
    elsif category == "high_to_low"
        filter_high_to_low.click
    end
  end

  def add_product_to_cart(product)
    find(:xpath, "//*[contains(text(),'#{product}')]").click
    add_to_cart_button.click
  end
end