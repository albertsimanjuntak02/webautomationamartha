# frozen_string_literal: true

class CheckoutPage < SitePrism::Page
  element:quantity_field, :xpath, "//*[@class='cart_quantity']"
  element:checkout_button, :xpath, "//*[contains(text(), 'Checkout')]"
  element:input_first_name_field, :xpath, "//*[@id='first-name']"
  element:input_last_name_field, :xpath, "//*[@id='last-name']"
  element:input_postal_code_field, :xpath, "//*[@id='postal-code']"
  element:continue_button, :xpath, "//*[@id='continue']"
  element:finish_button, :xpath, "//*[@id='finish']"
  element:checkout_complete_txt, :xpath, "//*[contains(text(), 'Checkout: Complete')]"
  element:complete_header_txt, :xpath, "//*[contains(text(), 'THANK YOU FOR YOUR ORDER')]"
  def validate_product(product)
    find(:xpath, "//*[contains(text(),'#{product}')]")
  end

  def overview_product(product)
    find(:xpath, "//*[contains(text(),'#{product}')]")
    find(:xpath, "//*[contains(text(),'Payment Information')]")
    find(:xpath, "//*[contains(text(),'Shipping Information')]")
    find(:xpath, "//*[contains(text(),'Total')]")
  end
end